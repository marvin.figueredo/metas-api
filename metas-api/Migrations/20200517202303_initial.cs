﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace metas_api.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ExternalApplications",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ApplicationName = table.Column<string>(nullable: true),
                    ApplicationDescription = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExternalApplications", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Metas",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ApplicationId = table.Column<string>(nullable: true),
                    Titulo = table.Column<string>(nullable: true),
                    Descripcion = table.Column<string>(nullable: true),
                    Notas = table.Column<string>(nullable: true),
                    Porcentaje = table.Column<decimal>(nullable: false),
                    ParentId = table.Column<string>(nullable: true),
                    FechaAdd = table.Column<DateTime>(nullable: false),
                    FechaDelete = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Metas", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExternalApplications");

            migrationBuilder.DropTable(
                name: "Metas");
        }
    }
}
